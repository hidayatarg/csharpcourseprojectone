﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RecapProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //Since empty will give error

            string key = tbxSearch.Text.ToString();
            if (string.IsNullOrEmpty(key))
            {
                ListProducts();
            }
            ListProductsByName(key);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ListProducts();
            ListCategory();
            
        }

        private void ListProducts()
        {
            using (NorthwindContex contex = new NorthwindContex())
            {
                dgwProduct.DataSource = contex.Products.ToList();
            }
        }


        private void ListProductsByCategory(int categoryId)
        {
            using (NorthwindContex contex = new NorthwindContex())
            {
                dgwProduct.DataSource = contex.Products.Where(p=>p.CategoryId==categoryId).ToList();
            }
        }


        private void ListProductsByName(string productName)
        {
            using (NorthwindContex contex = new NorthwindContex())
            {
                dgwProduct.DataSource = contex.Products.Where(p => p.ProductName.Contains(productName)).ToList();
            }
        }

        private void ListCategory()
        {
            using (NorthwindContex contex = new NorthwindContex())
            {
                cbxCategory.DataSource = contex.Categories.ToList();
                cbxCategory.DisplayMember = "CategoryName";
                cbxCategory.ValueMember = "CategoryId";
            }
        }

        private void cbxCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //sadece hata verecek cunku ilk bu bosta olacaktir
          //  ListProductsByCategory(Convert.ToInt32(cbxCategory.SelectedValue));

            try
            {
                ListProductsByCategory(Convert.ToInt32(cbxCategory.SelectedValue));
            }
            catch
            {
                // ignored
            }
        }
    }
}
